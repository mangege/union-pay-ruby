# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'union-pay/version'

Gem::Specification.new do |spec|
  spec.name          = "union-pay-ruby"
  spec.version       = UnionPay::VERSION
  spec.authors       = ["mangege"]
  spec.email         = ["mr.mangege@gmail.com"]
  spec.summary       = %q{UPOP and UPMP SDK}
  spec.description   = %q{UPOP and UPMP SDK}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"
end
