module UnionPay
  autoload :VERSION, 'union-pay/version'
  autoload :Core, 'union-pay/core'
  autoload :Mobile, 'union-pay/mobile'
end
