require 'logger'

module UnionPay
  module Core
    module Logger
      def net_log(req, resp, notice=false)
        return unless @config.enable_log
        @net_log ||= Object.const_get(@config.net_log_class).new
        @net_log.log(req, resp, notice)
      end

      class NetLog
        def initialize
          if defined?(Rails)
            @logger = Rails.logger
          else
            @logger = ::Logger.new(STDOUT)
          end
        end

        def log(req, resp, notice=false)
          @logger.info("unionpay req: #{req.inspect} -- resp: #{resp.inspect} -- notice:#{notice.inspect}")
        end
      end
    end
  end
end
