require 'uri'

module UnionPay
  module Core
    class Utils
      class << self
        def key_to_sym(kv)
          kv.inject({}){|memo, (k,v)| memo[k.to_sym] = v; memo}
        end

        def check_hash_keys!(hash, *keys)
          keys.flatten.each do |key|
            raise ArgumentError.new("Unknown key: #{key.inspect}") unless hash.has_key?(key)
          end
        end

        def encode_reserved(kv)
          "{#{URI.encode_www_form(kv)}}"
        end

        def decode_reserved(str)
          return str unless str.is_a?(String)
          result = URI.decode_www_form(str[1..-2])
          result = Hash[result]
          key_to_sym(result)
        end
      end
    end
  end
end
