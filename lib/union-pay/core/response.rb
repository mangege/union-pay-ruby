require 'cgi'

module UnionPay
  module Core
    class Response
      attr_reader :body

      def initialize(resp, config)
        if resp.is_a?(String)
          @body = parse_query(resp)
        else
          @raw_body = ::UnionPay::Core::Utils.key_to_sym(resp)
          @body = @raw_body.dup
          decode_reserved!(@body)
        end
        @config = config
      end

      def success?
        sign_valid? && @body[:respCode] == '00'
      end

      def sign_valid?
        Signer.md5_sign(@raw_body, @config.secret_key) == @raw_body[:signature]
      end

      def [](key)
        @body[key]
      end

      def inspect
        @body.inspect
      end

      private
      def parse_query(query_string)
        h = CGI.parse(query_string)
        @raw_body = Hash[h.collect{|k, v| v.size <= 1 ? [k.to_sym, v.first] : [k.to_sym, v]}]
        h = @raw_body.dup
        decode_reserved!(h)
        h
      end

      def decode_reserved!(params)
        params.each do |k, v|
          params[k] = UnionPay::Core::Utils.decode_reserved(v) if k.to_s =~ /Reserved$/
        end
      end
    end
  end
end
