require 'uri'
require 'net/http'

module UnionPay
  module Core
    class HttpClient
      include ::UnionPay::Core::Logger

      def initialize(url, params, config)
        @uri = URI(url)
        @params = params
        @config = config
      end

      def post
        httpResp = Net::HTTP.post_form(@uri, @params)
        net_log(@params, httpResp.body)
        Response.new(httpResp.body, @config)
      end

      def self.post(url, params, config)
        self.new(url, params, config).post
      end
    end
  end
end
