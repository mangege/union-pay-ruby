require 'erb'
require 'yaml'

module UnionPay
  module Core
    class Config
      attr_reader :config
      def initialize(config)
        @config = config
      end

      def self.read_config_file(file_name)
        erb = ERB.new(File.read(file_name))
        erb.filename = file_name
        YAML.load(erb.result)
      end


      def self.mobile_config(file_name, env='development')
        config = read_config_file(file_name)['mobile'][env]
        config = Utils.key_to_sym(config)
        new(config)
      end

      %w[trade_url query_url merchant_id secret_key back_end_url front_end_url enable_log net_log_class].each do |att_name|
        define_method(att_name.to_sym)do
          @config[att_name.to_sym]
        end
      end
    end
  end
end
