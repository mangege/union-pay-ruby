require 'digest/md5'

module UnionPay
  module Core
    class Signer
      def self.md5_sign(params, secret_key)
        params = Utils.key_to_sym(params)
        params.delete(:signMethod)
        params.delete(:signature)
        str = params.sort.collect{|k, v| "#{k}=#{v}"}.join('&')
        Digest::MD5.hexdigest("#{str}&#{Digest::MD5.hexdigest(secret_key)}")
      end

      def self.sign!(params, secret_key=nil)
        params = params.delete_if{|k, v| v.nil? || (v.respond_to?(:empty?) && v.empty? )}
        params[:signature] = self.md5_sign(params, secret_key)
        params[:signMethod] = 'MD5'
        params
      end
    end
  end
end
