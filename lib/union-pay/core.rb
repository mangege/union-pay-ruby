module UnionPay
  module Core
    autoload :Signer, 'union-pay/core/signer'
    autoload :Config, 'union-pay/core/config'
    autoload :Utils, 'union-pay/core/utils'
    autoload :HttpClient, 'union-pay/core/http_client'
    autoload :Response, 'union-pay/core/response'
    autoload :Logger, 'union-pay/core/logger'
  end
end
