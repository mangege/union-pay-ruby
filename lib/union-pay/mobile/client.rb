require 'union-pay/core'

module UnionPay
  module Mobile
    class Client
      include ::UnionPay::Core::Logger

      attr_reader :config

      # trade_url, query_url, merchant_id, back_end_url, front_end_url, secret_key
      def initialize(config_or_file)
        if config_or_file.is_a?(String)
          @config = ::UnionPay::Core::Config.mobile_config(config_or_file)
        else
          @config = config_or_file
        end
      end

      # 默认为人民币 
      # 金额填原始值就行, 比如100元,传100过来就行
      def consume(other_params)
        UnionPay::Core::Utils.check_hash_keys!(other_params, [:orderTime, :orderNumber, :orderAmount])

        params = default_params
        params = params.merge(other_params)
        params = params.merge({
          transType: '01',
          orderTime: conver_time(other_params[:orderTime]),
          orderAmount: conver_amount(other_params[:orderAmount])
        })
        trade(params)
      end

      def undo(other_params)
        undo_and_refund(other_params.merge({transType: '31'}))
      end

      def refund(other_params)
        undo_and_refund(other_params.merge({transType: '04'}))
      end

      def async_notice(params)
        net_log(nil, params, true)
        ::UnionPay::Core::Response.new(params, @config)
      end

      def query(other_params)
        UnionPay::Core::Utils.check_hash_keys!(other_params, [:transType, :orderTime, :orderNumber])
        params = default_params
        params = params.merge(other_params)
        params[:orderTime] = conver_time(other_params[:orderTime])
        call_api(@config.query_url, params)
      end

      protected
      def default_params
        @_default_params ||= {
          version: '1.0.0',
          charset: 'UTF-8',
          merId: @config.merchant_id,
          backEndUrl: @config.back_end_url,
          frontEndUrl: @config.front_end_url,
        }
      end

      def trade(params)
        call_api(@config.trade_url, params)
      end

      def call_api(path, params)
        encode_reserved!(params)
        ::UnionPay::Core::Signer.sign!(params, @config.secret_key)
        ::UnionPay::Core::HttpClient.post(path, params, @config)
      end

      private
      def undo_and_refund(other_params)
        UnionPay::Core::Utils.check_hash_keys!(other_params, [:orderTime, :orderNumber, :qn, :orderAmount])
        params = default_params
        params = params.merge(other_params)
        params = params.merge({
          orderTime: conver_time(other_params[:orderTime]),
          orderAmount: conver_amount(other_params[:orderAmount])
        })
        trade(params)
      end

      def conver_amount(amount)
        (amount * 100).ceil
      end

      def conver_time(dt)
        if dt.respond_to?(:strftime)
          dt.strftime('%Y%m%d%H%M%S')
        else
          dt
        end
      end

      def encode_reserved!(params)
        params.each do |k, v|
          params[k] = UnionPay::Core::Utils.encode_reserved(v) if k.to_s =~ /Reserved$/
        end
      end
    end
  end
end
