module UnionPay
  module Mobile
    autoload :Client, 'union-pay/mobile/client'
    autoload :TradeTypes, 'union-pay/mobile/trade_types'
  end
end
