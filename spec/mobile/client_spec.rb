require 'spec_helper'

include UnionPay::Mobile

describe Client do
  before(:each) do
    @client = Client.new('spec/config/unionpay.yml')
  end

  it "创建消费订单成功" do
    params = {
      orderTime: Time.now,
      orderNumber: Time.now.to_i,
      orderAmount: 100
    }
    resp  = @client.consume(params)
    expect(resp.success?).to be_true
  end

  it "消费订单的异步通知" do
    msg = {"orderTime"=>"20140603120329", "settleDate"=>"0602", "orderNumber"=>"eb2a20002be770f948dbce6341f6c0ba40af9d9c", "exchangeRate"=>"0", "signature"=>"ea315b1cc406c2b2c6caa3cf49c60a5b", "settleCurrency"=>"156", "signMethod"=>"MD5", "transType"=>"01", "respCode"=>"00", "charset"=>"UTF-8", "sysReserved"=>"{traceTime=0603120329&acqCode=00215800&traceNumber=094215}", "version"=>"1.0.0", "settleAmount"=>"37800", "transStatus"=>"00", "merId"=>"880000000001633", "qn"=>"201406031203290942151"}
    resp = @client.async_notice(msg)
    expect(resp.success?).to be_true
  end

  it "透传信息应该原样返回" do
    params = {
      orderTime: Time.now,
      orderNumber: Time.now.to_i,
      orderAmount: 100,
      reqReserved: {order_id: 1}
    }
    resp  = @client.consume(params)
    expect(resp[:reqReserved][:order_id]).to eq('1')
  end

  it "查询" do
    params = {
      orderTime: Time.now,
      orderNumber: Time.now.to_i,
      orderAmount: 100,
      reqReserved: {order_id: 1}
    }
    @client.consume(params)

    resp = @client.query({orderTime: params[:orderTime], orderNumber: params[:orderNumber], transType: '01'})
    expect(resp[:respCode]).to be_true
  end
end
