require 'spec_helper'

describe UnionPay::Core::Utils do
  it "encode_reserved" do
    result = UnionPay::Core::Utils.encode_reserved({a: 1, b: '2='})
    expect(result).to eq('{a=1&b=2%3D}')
  end

  it "decode_reserved" do
    result = UnionPay::Core::Utils.decode_reserved('{a=1&b=2%3D}')
    expect(result).to eq({a: '1', b: '2='})
  end
end
