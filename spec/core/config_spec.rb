require 'spec_helper'

describe UnionPay::Core::Config do
  it "读取文件" do
    expect(UnionPay::Core::Config.read_config_file('spec/config/unionpay.yml')).to be_include('mobile')
  end

  it "mobile config" do
    mc = UnionPay::Core::Config.mobile_config('spec/config/unionpay.yml')
    expect(mc.trade_url).to be_true
  end
end
