require 'spec_helper'

include UnionPay::Core

describe Signer do
  it "返回签名" do
    params = {key1: 2, key3: 1, key2: 5}
    expect(Signer.md5_sign(params, 'abc')).to eq('6ded5f60164343a7721eae2947d503c3')
  end

  it "signMethod和signature不参与签名" do
    params = {key1: 2, key3: 1, key2: 5, signMethod: 'MD5', signature: '321'}
    Signer.sign!(params, 'abc')
    expect(params[:signature]).to eq('6ded5f60164343a7721eae2947d503c3')
  end

  it "value为空的不参与签名" do
    params = {key1: 2, key3: 1, key2: 5, key4: '', key5: nil}
    Signer.sign!(params, 'abc')
    expect(params[:signature]).to eq('6ded5f60164343a7721eae2947d503c3')
  end
end
